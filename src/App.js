import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Axios from 'axios';

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      user: [],
      store: []
    }
  }

  componentDidMount() {
    var config = {
      headers: {'Access-Control-Allow-Origin': '*'}
    };
    Axios.get('http://3.94.105.4/api/', config)
      .then((data) => {
        this.setState({
          store: data.data
        })
      })
      .catch((error) => {
        console.log(error)
      })
  }

  result = (data) => {
    const e = data || []
    const result = e.map( r => (
      <tr key={r.id}>
        <td>{r.country}</td>
        <td>{r.province}</td>
        <td>{r.place}</td>
        <td>{r.condition}</td>
        <td>{r.temperature}</td>
        <td>{r.unit}</td>
        <td>{r.zipCode}</td>
      </tr>
    ))

    return result;
  }

  button = (e) => {
    Axios.post('http://3.94.105.4/api', {
      zipcode: document.getElementById('zipcode').value
    }).then((data) => {
      this.setState({
        store: data.data
      })
    })
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <div>
          <table>
            <tbody>
              <tr>
                <td>Country</td>
                <td>Province</td>
                <td>City</td>
                <td>Condition</td>
                <td>Temparature</td>
                <td>Unit</td>
                <td>Zipcode</td>
              </tr>
              {
                this.result(this.state.store)
              }
            </tbody>
          </table>
        </div>
        <div>
          <input id="zipcode" type="text" placeholder="Zipcode" /> 
          <button onClick={this.button}>Submit</button>
        </div>
      </div>
    );
  }
}

export default App;
